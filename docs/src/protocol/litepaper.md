# Litepaper

## Abstract

Lasso Finance is creating easy-to-use and a compliant bridge between DeFi and CeFi. Lasso wraps complicated DeFi technologies to come up with a simple solutions for Centralized Exchanges(CEXs) to participate in DeFi products.

## Method

 We are a bunch of DeFi users ourselves. We have gone through the UX of dozens of wallets, uncountable number of complicated user flows, and paid hundreds in gas fees to develop Lasso. Was this hard work? Absolutely, yes! Do we recommend others do the same? Not necessarily.

## Motivation

All retail investors start their crypto journey by buying crypto assets (mostly BTC) through CEXs. This is a hastle free, custodian friendly, and most importantly compliant path to acquiring major crypto assets. Retail investors trust their exchanges because they are legally liable and financially insured.

Savvy retail investors convert their crypto assets into DeFi enabled platforms (mostly ETH) and start dabbling into various yield products. This is a complex, confusing, error-prone, risky ... (i'm running out of adjectives here) process.

Lasso makes this process simpler by creating a user experience that is directly integrated in the trusted CEXs. With just a couple of clicks, users will be able to start generating yield for their assets hosted on the CEXs.

Our approach is simple; we engage with our users at the top of the funnel during asset purchase by offering yield products. This strategy is a no-brainer for hodlers.

We are not limiting this service to CEXs only, but also opening up to OTC desks, custodians, and carefully selected financial advisors.

Starting with a simple PoS solution for Ethereum, but we have an extensive pipeline of DeFi products; for example, CDPs, Liquidity Pools, Insurances, etc.

## Making Staking Simple

### Removing financial barrier to entry

Staking for ETH2.0, minimum of 32 ETHs are required to qualify and run a validator node. At $2000 USD/ETH price, minimum requirement is $64,000 USD. This barrier limits opportunities for the general public to participate.

### Eliminating technical barrier to entry

Setting up a validator node requires a basic knowledge of computer programming and understanding of server management. This complexity is manageable, but constantly monitoring the servers, securing the keys, applying validator updates is a time consuming and stressful task. We provide a stress-free solution that does not require you to fire any commands in the terminal.

### Making assets liquid

When running a validator one must lock in 32 ETHs that can be claimed once ETH 2.0 is launched (early-mid 2022, if all goes as planned). This locked ETH value can become volatile and can end up being locked for a longer period of time. A sophisticated investor would want a liquid asset to be able to rebalance their portfolio. Lasso solves this issue by wrapping the locked ETH and providing L-ETH that can be traded in supported markets.
### Remixing DeFi 

Most importantly, L-ETH is just another DeFi product and fits pretty well with other DeFi products. We're working with other protocols to create liquidity pools, lending/borrowing products, DEX integration so that L-ETH can be used as an additional yield generating instrument.

## L-ETH Flows

L-ETH has six(6) major flows;

1. Onboarding Exchanges
1. Exchange needs to wrap ETH to L-ETH.
1. User need to purchase L-ETH or convert their ETH to L-ETH using the exchange.
1. User can use the L-ETH for DeFi purposes.
1. User can swap L-ETH to ETH back using the exchange.
1. Exchange can unwrap L-ETH to ETH (once ETH 2.0 allows to do so.)

### Exchange Onboarding  

![image](/assets/img/user-flows/deck-exchange-onboarding.png)

### Exchange swapping L-ETH to ETH

![image](/assets/img/user-flows/deck-user-acquire-l-eth.png)

### Users acquiring L-ETH

![image](/assets/img/user-flows/deck-user-acquire-l-eth.png)

## Governance

We have adopted a progressive decentralization approach. Currently, all decisions are made by the Lasso team, but we are constantly discussing how to make the project more decentralized.

There are three types of users in the current governance.

### Admins

Admins include selected executives from the Lasso team. They can manage the smart contract, upgradeability, members, and staking related functions.

### Members

Members are carefully vetted and approced CEXs and OTC desks. They can wrap ETH to L-ETH and vice-versa.

Members are managed by the Admins. Once a CEX or an OTC desk is approved, they supply a list of Ethereum addresses that belong to them. Admin adds the list of addresses to the members group. Going forward any of the addresses supplied by the members can do such tasks.

If one of the member addresses is compromised, the CEX or the OTC desk can request to remove or replace that address with a different one.

Aim is to create a up-to-date list of compliant Ethereum addresses that can interact with the key functionalities of the contract.

### Public

Public is everyone else. They can do all `view` functions and variables along side with standard ERC20 functions.

## Security

We are using standard security libraries and protocols to secure the validators and the smart contracts. For any concerns around our security, feel free to reach out to us.
