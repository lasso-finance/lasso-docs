# Token Interface

## Standard ERC20 Token

All of these standard ERC20 Token functions are suppoe
```
    function totalSupply() constant returns (uint theTotalSupply);
    function balanceOf(address _owner) constant returns (uint balance);
    function transfer(address _to, uint _value) returns (bool success);
    function transferFrom(address _from, address _to, uint _value) returns (bool success);
    function approve(address _spender, uint _value) returns (bool success);
    function allowance(address _owner, address _spender) constant returns (uint remaining);
    event Transfer(address indexed _from, address indexed _to, uint _value);
    event Approval(address indexed _owner, address indexed _spender, uint _value);
```

## Additional Features

All the additional features divided into three tiers of access. Users are described in the [Litepaper](/protocol/litepaper.html#governance)

### Admin Functions

Supported for only `Admin` users defined in the [governance](/protocol/litepaper.html#admins)

```
    function addMember(address newMember)
    function removeMember(address oldMember)
    function getWallet() public view onlyAdmin() returns(address)
    function setWallet(address payable wallet)
    function transferToWallet()
    function pause()
    function unpause()
    function rebase(uint256 reward, uint currentEpoch)
```

### Members

Supported for `Admin` users and `Member` users defined in the [governance](/protocol/litepaper.html#members). 

#### Wrap L-ETH to ETH

```
    function wrap(uint256 amount) public payable
```

Members send `32` or `multiple of 32` ETHs to wrap into LETH. `amount` and the ETH value sent to the smart contract MUST match in order to continue.

Upon succesful execution of this function, the `sender` will receive the newly minted L-ETH equal value of `amount`.

#### Unwrap ETH to L-ETH

This function acts opposite of `wrap` function, but is currently not supported due to technical limitations of ETh2.0.

### Public
Supported for all `public` users defined in the [governance](/protocol/litepaper.html#public)

#### Get Last Epoch

```
    function getLastEpoch() public view returns(uint256)
```
This shows the last Epoch when the ETH2.0 deposit was made. This can be used as a marker to trigger calculations.

#### Get LETH Pool

```
    function getLethPool() public view returns(uint256)
```
This returns the total amount of L-ETH wrapped by the contract. Often used with `getETH2Pool` to calculate the L-ETH value in ETH.

#### Get ETH2 Pool

```
    function getEth2Pool() public view returns(uint256)
```

This returns the total amount of ETH2 in the validator pools. It includes the staked ETHs and rewards generated using them. This function is often used with `getLethPool()` to calculate the L-ETH value in ETH.
