---
home: true
heroImage: /assets/img/hero-logo.png
actionText: Getting Started →
actionLink: /intro/
# features:
# - title: Feature 1 Title
#   details: Feature 1 Description
# - title: Feature 2 Title
#   details: Feature 2 Description
# - title: Feature 2 Title
#   details: Feature 2 Description
# - title: Feature 2 Title
#   details: Feature 2 Description
footer: Made by Lasso Finance with ❤️
---
