const { description } = require('../../package')

module.exports = {
  /**
   * Ref：https://v1.vuepress.vuejs.org/config/#title
   */
  title: 'Lasso Developers',
  /**
   * Ref：https://v1.vuepress.vuejs.org/config/#description
   */
  description: "Developer Documentation",

  /**
   * Extra tags to be injected to the page HTML `<head>`
   *
   * ref：https://v1.vuepress.vuejs.org/config/#head
   */
  head: [
    ['meta', { name: 'theme-color', content: '#3eaf7c' }],
    ['meta', { name: 'apple-mobile-web-app-capable', content: 'yes' }],
    ['meta', { name: 'apple-mobile-web-app-status-bar-style', content: 'black' }]
  ],

  /**
   * Theme configuration, here is the default theme configuration for VuePress.
   *
   * ref：https://v1.vuepress.vuejs.org/theme/default-theme-config.html
   */
  themeConfig: {
    repo: '',
    editLinks: false,
    docsDir: '',
    editLinkText: '',
    lastUpdated: false,
    logo: '/assets/img/logo.png',
    nav: [
      {
        text: 'Docs',
        link: '/intro/',
      },
      {
        text: 'Company',
        link: 'https://lasso.finance/'
      }
    ],
    sidebar: [
      {
        title: "Introduction",
        collapsable: false,
        path: '/intro/'
      },
      {
        title: "Protocol",
        collapsable: false,
        path: '/protocol/litepaper',
        children: [
          {
            path: '/protocol/litepaper',
            title: 'Litepaper',
            sidebarDepth: 2,
            collapsable: true
          },
          '/protocol/glossary',
          '/protocol/faqs'
        ],
      },
      {
        title: "Technology",
        collapsable: false,
        path: '/technology/token-architecture',
        sidebarDepth: 0,
        children: [
          '/technology/token-architecture',
          // {
          //   path: '/technology/deployed-contracts',
          //   title: 'Deployed Contracts',
          // }

        ],
      },
      {
        title: "Guides & Tutorials",
        collapsable: false,
        path: '/guides/how-to-check-leth-value',
        sidebarDepth: 0,
        children: [
          '/guides/how-to-check-leth-value',
        ],
      },
      {
        title: "Contact",
        collapsable: false,
        path: '/contact/',
      },
    ],
   },

  /**
   * Apply plugins，ref：https://v1.vuepress.vuejs.org/zh/plugin/
   */
  plugins: [
    '@vuepress/plugin-back-to-top',
    '@vuepress/plugin-medium-zoom',
  ]
}
