# How to calculate L-ETH value?



### Step 0: Import the libs

```
const { BigNumber, ethers } = require("ethers");
```

### Step 1: Connect with the contract

```
# Goerli contract address: 
const contractAddress = 0x4C41E20234dDF167e0726440Da35C22f3117D487;

# Mainnet contract address: 
const contractAdress = 0xfc50dF01aAaa4C7789760C8286061F6Af7f7E038;


const contractAbi = require('./contractABI.json');
ethers.Contract( contractAddress , contractAbi , signerOrProvider );
```

Latest contract ABI can be found [here](./contract-abi-json).

### Step 2: Get total L-ETH pool & Get total ETH2 pool

```
const LETHPool = await token.getLethPool();
const ETH2Pool = await token.getEth2Pool();    
```

### Step 3: Value of L-ETH

```
const BigLETHPool = BitNumber.from(LETHPool);
const BigETH2Pool = BitNumber.from(ETH2Pool);

const LETHValue = BigLETHPool / BigETH2Pool;
```

Note: Value of LETH in Fiat can be dervied from the ETH value Fiat.

## Full Script (coming soon)
