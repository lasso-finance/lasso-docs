# Welcome to Lasso

Lasso is offering DeFi products to compliant crypto entities like exchanges, OTC desks, and custodians. This website lists different ways that developers can integrate with Lasso products.

Next steps:

- Checkout our [Litepaper](/protocol/litepaper)
- Checkout our [Technology](/technology/token-architecture)
- Checkout our [Integration Guide](/guides/how-to-check-leth-value)
